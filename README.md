# Fancynote

This is an experiment! It's a small 0 dependency typescript program to process
notes written in an experimental lisp dialect.

Experimental here doesn't mean anything particularly interesting, just that I'm
experimenting with it! No crazy features, nothing fancy. 

- `./src` contains the typescript libraries for parsing and generating html
- `./notes` contains notes
