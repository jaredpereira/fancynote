import {AST} from './parser'

let Elements:{[k:string]: (ast: AST)=> string} = {
  "!": (ast)=> el('p', {}, toHTMLSubtree(ast)),
  "\n": ast => {
    return ast.length > 0 ? el('p', {}, toHTMLSubtree(ast)) : ''
  },
  "i": (ast)=> el('i', {}, toHTMLSubtree(ast)),
  "b": ast => el('b', {}, toHTMLSubtree(ast)),
  "q": ast => el('blockquote', {}, toHTMLSubtree(ast)),
  "l": (ast) => {
    let source = ast[ast.length - 1]
    if(!source || source.type === 'list') throw Error('no source for link')
    if(source.value.endsWith('.fn')) source.value = source.value.slice(0, -3) + '.html'
    if(ast.length === 1) return el('a', {href: source.value}, source.value.split('.')[0])
    return el('a', {href: source.value}, toHTMLSubtree(ast.slice(0, -1)))
  },
  "list": ast=> el('ul', {}, toHTMLSubtree(ast)),
  "olist": ast=> el('ol', {}, toHTMLSubtree(ast)),
  "-": ast => el('li', {}, toHTMLSubtree(ast)),
  '---': _ast => '<hr/>',
  "code": ast => el('pre', {}, toHTMLSubtree(ast)),
  "#": (ast)=> el('h1', {}, toHTMLSubtree(ast)),
  "##": (ast)=> el('h2', {}, toHTMLSubtree(ast)),
  "###": (ast)=> el('h3', {}, toHTMLSubtree(ast)),
}

export function toHTML (ast:AST) {
  return  `
<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8"/>
<title>fancynote test</title>
<style>
html {
font-size: 16px;
line-height: 1.5;
}
body {max-width: 700px; margin: auto;}
a:visited {color: blue;}
blockquote {
  border-left: 4px solid;
  padding-left: 16px;
  margin-left: 32px;
}
</style>

<body>
<br/>
<a href="/">index page</a> - <a href='https://awarm.space'>awarm.space</a>
<br/>
${toHTMLSubtree(ast)}
</body>
`
}

export function toHTMLSubtree(ast:AST):string {
  if(!ast[0]) return ''
  if(ast[0].type === 'atom') {
    let command = Elements[ast[0].value]
    if(command) return command(ast.slice(1))
  }

  let result = ''
  for(let i=0; i<ast.length; i++) {
    let thing = ast[i]
    let next = ast[i +1]
    if(thing.type === 'list') result += toHTMLSubtree(thing.children)
    else {
      if(thing.value === '\n' && next?.type === 'atom' && next.value === '\n') {
        result += '<p>'
        i++
      }
      else result += thing.value + ' '
    }
  }
  return result
}

function el(tag: string, attrs: {[key: string]:string}, children: string): string {
  let attrsString = Object.keys(attrs).map(key =>{
    return `${key}="${attrs[key]}"`
  }).join(' ')
  return `<${tag} ${attrsString}>${children}</${tag}>`
}
