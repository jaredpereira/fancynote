import {AST} from './parser'
export function astToString(ast: AST): string {
  let result = ''
  for(let node of ast) {
    if(node.type === 'atom') {result += ' ' +  node.value}
    else result += `[${astToString(node.children)}]`
  }
  return result
}
