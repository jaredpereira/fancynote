import fs from 'fs'
import path from 'path'
import {parser} from './parser'
import {toHTML} from './astToHTML'
import { astToString } from './astToString'

let folderToHtml = (dir: string)=> {
  let fullPath = path.join(__dirname, "../../notes", dir)
  let outDirectory = path.join(__dirname, '../../out', dir)

  if(dir!== '' && !fs.existsSync(outDirectory)) fs.mkdirSync(outDirectory)

  let files = fs.readdirSync(fullPath)

  for(let file of files) {
    let filepath = path.join(fullPath, file)
    if(fs.lstatSync(filepath).isDirectory()) folderToHtml(path.join(dir, file))
    else {
      console.log(file + '-----------------')
      let input = fs.readFileSync(filepath).toString()
      let ast = parser(input)
      let html = toHTML(ast)
      let text = astToString(ast)

      fs.writeFileSync(path.join(outDirectory, file.slice(0, -3) + '.html'), html)
      fs.writeFileSync(path.join(outDirectory, file.slice(0, -3) + '.fn'), text)
    }
  }
}

folderToHtml('')
