import {AST} from './parser'
export function execute(ast: AST): AST {
  let output:AST = []
  let command = ast[0]

  if(command.type === 'atom' && command.value[0] === '!') {
    if(command.value === '!map') {
      console.log(ast)
      let array = ast[1]
      let mapFunction = ast[2]
      console.log(array)
      console.log(mapFunction)

      if(array.type === 'atom' || mapFunction.type === 'atom') throw Error()
      let variable = mapFunction.children[0]
      let template = mapFunction.children[1]

      if(template.type !== 'list') throw Error()
      if(variable.type !== 'atom') throw Error()

      const replace = (nodes:AST, value: AST[0]):AST=> {
        if(variable.type !== 'atom') throw Error()
        let result = []
        for(let node of nodes) {
          if(node.type === 'list') result.push({type:'list', children: replace(node.children, value)} as const)
          if(node.type === 'atom' && node.value === variable.value) result.push(value)
          else result.push(node)
        }
        return result
      }

      output = array.children.map(child => {
        if(template.type !== 'list') throw Error()
        return {type: 'list', children: replace(template.children, child)}
      })
    }
  }
  else {
    for(let node of ast) {
      if(node.type === 'list') output.push({type: 'list', children: execute(node.children)})
      else output.push(node)
    }
  }
  return output
}
