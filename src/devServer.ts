import fs from 'fs'
import path from 'path'
import http from 'http'

import {parser} from './parser'
import {execute} from './compile'
import {toHTML} from './astToHTML'

http.createServer(function (req, res) {
  let page = req.url || ''
  if(page.endsWith('/')) page = page + 'index.fn'
  else if(page.endsWith('.html')) page = page.slice(0, -5) + '.fn'
  else page = page + '.fn'

  fs.readFile(path.join(__dirname, '../../notes',  page), function (err,data) {
    if (err) {
      res.writeHead(404);
      res.end(JSON.stringify(err));
      return;
    }
    res.writeHead(200, {'Content-Type': 'text/html'})
    res.end(Buffer.from(fileToHtml(data.toString())))
  });
}).listen(8080);

console.log('Listening on port 8080')

let fileToHtml = (input: string) => {
  let ast = parser(`[${input}]`)
  let html = toHTML(execute(ast))
  return html
}
