;;; fancynote.el --- description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2020 Jared Pereira
;;
;; Author: Jared Pereira <http://github/jared>
;; Maintainer: Jared Pereira <jared@awarm.space>
;; Created: May 29, 2020
;; Modified: May 29, 2020
;; Version: 0.0.1
;; Keywords:
;; Homepage: https://github.com/jared/fancynote
;; Package-Requires: ((emacs 26.3) (cl-lib "0.5"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  description
;;
;;; Code:



(provide 'fancynote)


(defconst fancynote-mode-syntax-table (let ((table (make-syntax-table)))
                                        (modify-syntax-entry ?\[ "(]" table)
                                        (modify-syntax-entry ?\] ")]" table)
                                        table))

(add-to-list 'auto-mode-alist '("\\.fn\\'" . fancynote-mode))

(define-derived-mode fancynote-mode nil "Fancynote"
  "major mode for editing fancynote files."
  :syntax-table fancynote-mode-syntax-table
  (set (make-local-variable 'indent-line-function) 'indent-relative)
  (display-line-numbers-mode)
  (auto-fill-mode)
  )


;;; fancynote.el ends here
