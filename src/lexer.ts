export type Token = {
  value: string
}

const specials = [
  '[',
  ']',
  ' ',
  '\n'
]
export function lexer (input: string):Token[] {
  let tokens:Token[] = []
  let position = 0
  let rest = input

  const bite = (num:number) => {
    tokens.push({value: rest.slice(0, num)})

    rest=rest.slice(num)
    position += num
  }

  const flip = (num: number) => {
    tokens.push({value: "["})
    bite(num)
    skip()
  }

  const skip = () => {
    position++
    rest = rest.slice(1)
  }

  while(position < input.length) {
    let char = input[position]
    switch(char) {
      case '[': {
        bite(1)
        break
      }

      case ']': {
        bite(1)
        break
      }
      case ' ':
        skip()
        break
      case '\n': {
        bite(1)
        break
      }
      case '`': {
        parseCodeBlock()
        break
      }
      default: {
        parseAtom()
      }
    }
  }

  return tokens

  function parseAtom() {
    for(let i=0; i < rest.length; i++) {
      if(rest[i]==='[') return flip(i)
      if(specials.includes(rest[i])) return bite(i)
    }
    return bite(rest.length)
  }

  function parseCodeBlock() {
    skip()
    let endBlock = rest.indexOf('`')
    if(endBlock === -1) throw new Error('Unterminated codeblock')

    bite(endBlock)
    skip()
    return
  }
}
