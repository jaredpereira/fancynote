import { Token, lexer} from './lexer'
type Atom = { type: 'atom', value: string }
type List = { type: 'list', children: Array<Atom | List>}
export interface AST extends Array<Atom | List> {}

function generateSubtree(input:Token[]): [Atom | List, number] {
  let AST:AST = []
  if(input[0].value !== "[") return [{type: 'atom', value: input[0].value}, 1]

  for(let i = 1; i < input.length;){
    let token = input[i].value
    if(token === ']') return [{type: 'list', children: AST}, i+1]
    let [block, length]= generateSubtree(input.slice(i))
    AST.push(block)
    i += length
  }
  throw Error('input does not terminate')
}

export function parser(input:string):AST {
  let tokens = lexer(input)
  let output:AST = []
  let position = 0
  while(position < tokens.length) {
    let [node, lengthParsed] = generateSubtree(tokens.slice(position))
    output.push(node)
    position += lengthParsed
  }
  return output
}
