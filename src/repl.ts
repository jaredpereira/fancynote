import readline from 'readline'
import { parser } from './parser';
import {execute} from './compile'
import { astToString } from './astToString';

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  prompt: '> '
});

rl.prompt()

let input = ''

rl.on('line', (line)=>{
  input += line
  try {
    let ast = parser(input)
    let newAST = execute(ast)
    let text = astToString(newAST)
    console.log(text)
    input = ''
    rl.prompt()
  }
  catch (e) {
    input += "\n"
    rl.prompt()
  }
})
